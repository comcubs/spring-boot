//package com.example.postgresdemo.model2;
//
////import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;
//
//import javax.persistence.*;
//import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Size;
//
//@Entity
//@Table(name = "Register")
//public class Register {
//    @Id
//    @GeneratedValue(generator = "register_generator")
//    @SequenceGenerator(
//            name = "register_generator",
//            sequenceName = "register_sequence",
//            initialValue = 1000
//    )
//    private Long id;
//
//    @NotBlank
//    @Size(min = 3, max = 100)
//    private String name;
//
//    // @Column(columnDefinition = "email")
//    //private String email;
//
//    @Column(columnDefinition = "password")
//    private String userPassword;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    //public String getEmail() {
//    //    return email;
//    // }
//
//    //public void setEmail(String email) {
//    //    this.email = email;
//    // }
//
//    public String getUserPassword() {
//        return userPassword;
//    }
//
//    public void setUserPassword(String userPassword) {
//        this.userPassword = userPassword;
//    }
//}

package com.example.postgresdemo.model2;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "auth_user")
public class Register {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auth_user_id")
    private int id;

    @NotBlank(message = "Mandatory")
    @Size(min = 2, max = 100)
    @Column(name = "Name", nullable = false)
    private String name;
    @NotNull(message="Email is compulsory")
    @Email(message = "Email is invalid")
    @Column(name = "email")
    private String email;

    @NotNull(message="Password is compulsory")
    @Length(min=5, message="Password should be at least 5 characters")
    @Column(name = "password")
    private String password;


//    @Column(name = "status")
//    private String status;


//    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.DETACH})
//    @JoinTable(name = "auth_user_role", joinColumns = @JoinColumn(name = "auth_user_id"), inverseJoinColumns = @JoinColumn(name = "auth_role_id"))
//    private Set<role> roles = new HashSet<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public Set<role> getRoles() {
//        return roles;
//    }
//
//    public void setRoles(Set<role> roles) {
//        this.roles = roles;
//    }




}