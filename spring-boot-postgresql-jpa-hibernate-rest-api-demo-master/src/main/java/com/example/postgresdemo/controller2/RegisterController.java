package com.example.postgresdemo.controller2;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model2.Register;
import com.example.postgresdemo.repository2.RegisterRepository;
//import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
public class RegisterController {

    @Autowired
    private RegisterRepository registerRepository;

    @GetMapping("/register")
    public Page<Register> getUsers(Pageable pageable) {
        return registerRepository.findAll(pageable);
    }


    @PostMapping("/register")
    public Register createUser(@Valid @RequestBody Register user) {
        return registerRepository.save(user);
    }

  @PutMapping("/register/{Id}")
    public Register updateUser(@PathVariable Integer Id,
                               @Valid @RequestBody Register userRequest) {
        return registerRepository.findById(Id)
                .map(user -> {
                    user.setEmail(userRequest.getEmail());
                    user.setPassword(userRequest.getPassword());
                    return registerRepository.save(user);
                }).orElseThrow(() -> new ResourceNotFoundException("User not found with id " + Id));
    }


    @DeleteMapping("/register/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer Id) {
        return registerRepository.findById(Id)
                .map(user -> {
                    registerRepository.delete(user);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("User not found with id " + Id));
    }
}
