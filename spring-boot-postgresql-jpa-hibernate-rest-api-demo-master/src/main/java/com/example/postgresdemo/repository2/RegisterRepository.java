//package com.example.postgresdemo.repository2;
//
//import com.example.postgresdemo.model2.Register;
////import com.example.postgresdemo.model2.Register;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface RegisterRepository extends JpaRepository<Register, Long> {
//}


package com.example.postgresdemo.repository2;

import java.util.List;
import java.util.Set;

import com.example.postgresdemo.model2.Register;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//import com.example.postgresdemo.model2.role;
import com.example.postgresdemo.model2.Register;
@Repository
public interface RegisterRepository extends JpaRepository<Register, Integer> {
   // public Register findByEmail(String email);
 //   public List<Register> findByRolesIn(Set<role> roles);

}
